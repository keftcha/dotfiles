#!/usr/bin/env bash

set -e

if [[ -n $(pactl get-sink-mute @DEFAULT_SINK@ | grep "yes") ]]; then
    pactl set-sink-mute @DEFAULT_SINK@ 0
else
    pactl set-sink-volume @DEFAULT_SINK@ -10%
fi
